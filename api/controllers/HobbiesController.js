/**
 * HobbiesController
 *
 * @description :: Server-side logic for managing hobbies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    create: (req, res) =>{
        let user = req.session.userId;
        const hobbies = req.param('hobbies');

        Hobbies.create({user, hobbies})
        .exec((error, hobby)=> {
            if (error) return res.serverError(error)
            sails.log.info('Created hobby', hobby)
            if(hobby){
               User.find({
                   where: {id:user}
               }).exec(function(err, result){
                    if (err) { return res.serverError(err); }
                    console.log(result);
                    let number = result[0]['phoneNumber'];
                    let email = result[0]['email'];
                    User.sendSms(number.toString());
                    User.sendEmail(email.toString());
                    
               })
               
               return res.ok();
                /*
                User.query('SELECT user.email, user.phoneNumber FROM user WHERE user.id = query', 
                function(err, rawResult){
                    if (err) { return res.serverError(err); }
                    let result = JSON.stringify(rawResult);
                    console.log(result);
                    let phoneNumber = result[0]['phoneNumber']
                    let email = result[0]['email'];
                    console.log(phoneNumber);
                    User.sendSms();
                })
                
                */
            }
        })
    }
};

