module.exports = {
  login: (req, res) => {
    const { email, password } = req.allParams()
    
    User
      .findOne({email})
      .exec((error, user) => {
        if (error) return res.serverError(error)
        if (!user) return res.forbidden()

        User
          .checkIfPasswordIsValid(password, user, (error, isValid) => {
            if (error) return res.serverError(error)
            if (!isValid) return res.forbidden()

            sails.log.info('User logged in', user)
            //check the db for the other one
            const encryptedId = CryptographyService.encrypt(user.id)
            req.session.userId = user.id
            console.log(req.session.userId);

            return res.redirect('/index.html')

           /*return res.json({
              id: user.id,
              token: TokenService.issue({id: user.id}),
              cookie: encryptedId
            })
            */
            
          
          })
      })
  }
}