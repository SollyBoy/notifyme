module.exports = {
    serve: (res, req) =>{
        if(!req.session){
             req.redirect('/homepage.html')
        }
         req.redirect('/index.html')
    },

    logout: (res, req) =>{
        if (!req.session) return req.redirect('/homepage.html')
        User.findOne(req.session, function foundUser(err, user) {
            if (err) return req.negotiate(err);
            if (!user) {
            sails.log.verbose('Session refers to a user who no longer exists.');
        }
        req.session = null;
        req.redirect('/homepage')
        });
    }
}