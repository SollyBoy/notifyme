/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    signup: (req, res) => {
    const { email, fullname, phoneNumber, password} = req.allParams()
    User
      .create({email, fullname, phoneNumber, password})
      .exec((error, user) => {
        if (error) return res.serverError(error)

        sails.log.info('Created user', user)
        //remove user from 
        if (user) {
          req.session.userId = user.id
          console.log(req.session.userId)
          return res.ok()
        }
      })
  }
}


