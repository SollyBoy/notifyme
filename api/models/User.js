/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
const bcrypt = require('bcryptjs')

const SparkPost = require('sparkpost');
const dlient = new SparkPost(sails.config.email.pass);

const client = require('twilio')(
  sails.config.twilio.accountID,
  sails.config.twilio.authToken
)

//clean if spark post works//
module.exports = {
  attributes: {
    fullname: {
      type: 'string',
      required: true
    },
    password: {
      type: 'string',
      required: true
    },

    email: {
    type: 'string',
    unique: 'true'
  },
  
    phoneNumber: {
    type: 'string'
  },

  hobby:{
    collection: 'hobbies',
    via: 'user'
  },
  },

  customToJSON () {
    let user = this.toObject()

    delete user.password

    return user
  },

  beforeCreate: (user, next) => {
    bcrypt.genSalt(10, (error, salt) => {
      if (error) return next(error)

      bcrypt.hash(user.password, salt, (error, hash) => {
        if (error) return next(error)
        user.password = hash
        next()
      })
    })
  },
  
  sendSms: (telNo) => {
    client.messages.create({
      from: sails.config.twilio.phoneNumber,
      to: telNo,
      body: 'Hobby has been added',
    }).then((message) => console.log(message.sid));
  },

 
//sparkpost//
 sendEmail: (email) => {
    dlient.transmissions.send({
    options: {
      sandbox: false
    },
    content: {
      from: 'akinsulire@lanre.co',
      subject: 'Notification!',
      html:'<html><body><p>Hobby has been added!</p></body></html>'
    },
    recipients: [
      {address: email}
    ]
  })
  .then(data => {
    console.log('Email Sent');
    console.log(data);
  })
  .catch(err => {
    console.log('Whoops! Something went wrong');
    console.log(err);
  });
 },

  checkIfPasswordIsValid: (password, user, callback) => {
    bcrypt.compare(password, user.password, (error, isMatch) => {
      if (error) callback(error)

      if (isMatch) {
        callback(null, true)
      } else callback(error, false)
    })
  }
}

