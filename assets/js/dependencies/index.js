
new Vue({
    el: '#hobbies',
     data:{
        oldHobbies: ['Eating','Dancing', 'playing'],
        hobbies: [],
        newHobbies: '',
    },


    methods:{
        addHobbies: function () {
            let text;
            text = this.newHobbies.trim();
            if (text) {
                this.hobbies.push({
                text: text
                });
                this.newHobbies = '';
                axios.post('/hobby',{
                    params:{
                        hobbies: text 
                    }
                })
                .then(function(response){
                    console.log(response)
                }).catch(function(error){
                    console.log(error)
                })
            }
        },
        removeTag(index) {
            //this.tags.splice(index, 1);
            this.oldHobbies.splice(index, 1);
        },
    }
});

