Vue.component('signup-component', {

    template: '#signup-template',
    
    data() {
        return {
            user: {
                fullname: '',
                email: '',
                telNo: '',
                password: ''
            },
            submitted: false,
        }
    },

    methods: {
        handleSubmit(){
            axios.post('/user/register', {
                fullname: this.user.fullname,
                email: this.user.email,
                phoneNumber: this.user.telNo,
                password: this.user.password,
            })
            .then(function (response){
                window.location.replace("login.html");   
            })
            .catch(function (error){
                console.log(error);
            });
        }
    }
});

var app2 = new Vue({
   el: '#app2'
});
