/**
 * Connections
 * (sails.config.connections)
 *
 * `Connections` are like "saved settings" for your adapters.  What's the difference between
 * a connection and an adapter, you might ask?  An adapter (e.g. `sails-mysql`) is generic--
 * it needs some additional information to work (e.g. your database host, password, user, etc.)
 * A `connection` is that additional information.
 *
 * Each model must have a `connection` property (a string) which is references the name of one
 * of these connections.  If it doesn't, the default `connection` configured in `config/models.js`
 * will be applied.  Of course, a connection can (and usually is) shared by multiple models.
 * .
 * Note: If you're using version control, you should put your passwords/api keys
 * in `config/local.js`, environment variables, or use another strategy.
 * (this is to prevent you inadvertently sensitive credentials up to your repository.)
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.connections.html
 */

module.exports.connections = {

  /***************************************************************************
  *                                                                          *
  * Local disk storage for DEVELOPMENT ONLY                                  *
  *                                                                          *
  * Installed by default.                                                    *
  *                                                                          *
  ***************************************************************************/

  /***************************************************************************
  *                                                                          *
  * MySQL is the world's most popular relational database.                   *
  * http://en.wikipedia.org/wiki/MySQL                                       *
  *                                                                          *
  * Run: npm install sails-mysql@for-sails-0.12 --save                       *
  *                                                                          *
  ***************************************************************************/
   MysqlServer: {
     adapter: 'sails-mysql',
     host: 'localhost',
     user: 'root', //optional
     password: 'francesc4$', //optional
     database: 'notify' //optional
   },
   
  /***************************************************************************
  *                                                                          *
  * MongoDB is the leading NoSQL database.                                   *
  * http://en.wikipedia.org/wiki/MongoDB                                     *
  *                                                                          *
  * Run: npm install sails-mongo@for-sails-0.12 --save                       *
  *                                                                          *
  ***************************************************************************/
  // someMongodbServer: {
  //   adapter: 'sails-mongo',
  //   host: 'localhost',
  //   port: 27017,
  //   user: 'username', //optional
  //   password: 'password', //optional
  //   database: 'your_mongo_db_name_here' //optional
  // },

  /***************************************************************************
  *                                                                          *
  * PostgreSQL is another officially supported relational database.          *
  * http://en.wikipedia.org/wiki/PostgreSQL                                  *
  *                                                                          *
  * Run: npm install sails-postgresql@for-sails-0.12 --save                  *
  *                                                                          *
  *                                                                          *
  ***************************************************************************/
 myPostgresqlServer: {
     adapter: 'sails-postgresql',
     host: 'localhost',
     database: 'notify',
     user: 'postgres',
     password: 'francesc4$'
   },


 /* myPostgresqlServer: {
     adapter: 'sails-postgresql',
     url: 'postgres://sttwlkwyexafqg:d11b0d060db62167e9989835248e042adab6ec0c0a71d699139f29f36961abeb@ec2-54-221-234-62.compute-1.amazonaws.com:5432/d2u2olvi5eo93f',
     ssl: true
   },
*/

    prodPostgresqlServer: {
     adapter: 'sails-postgresql',
     //url: 'process.env.DATABASE_URL',
     url: 'postgres://sttwlkwyexafqg:d11b0d060db62167e9989835248e042adab6ec0c0a71d699139f29f36961abeb@ec2-54-221-234-62.compute-1.amazonaws.com:5432/d2u2olvi5eo93f',
     ssl: true
   },


  /***************************************************************************
  *                                                                          *
  * More adapters: https://github.com/balderdashy/sails                      *
  *                                                                          *
  ***************************************************************************/

};
