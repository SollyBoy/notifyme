/**
 * Production environment settings
 *
 * This file can include shared settings for a production environment,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the production        *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/
   
   models: {
     connection: 'prodPostgresqlServer',
     //timeout: sails.config.grunt._hookTimeout
   },

  /* connections:{
     prodPostgresqlServer: {
     adapter: 'sails-postgresql',
     //url: 'process.env.DATABASE_URL',
     url: 'postgres://sttwlkwyexafqg:d11b0d060db62167e9989835248e042adab6ec0c0a71d699139f29f36961abeb@ec2-54-221-234-62.compute-1.amazonaws.com:5432/d2u2olvi5eo93f',
     ssl: true
   },
   */
  
  

  /***************************************************************************
   * Set the port in the production environment to 80                        *
   ***************************************************************************/

  port: process.env.PORT || 3000 

  /***************************************************************************
   * Set the log level in production environment to "silent"                 *
   ***************************************************************************/

  // log: {
  //   level: "silent"
  // }

};
